import Vue from "vue";

export const state = () => ({
  boards: [],
  board: {},
  comments: {},
  createBoardWindow: false,
})

export const mutations = {
  SET_BOARDLIST(state, boards) {
    state.boards = boards
  },

  SET_BOARD_USERS(state, users) {
    state.board.users = users
  },

  PUSH_BOARDLIST(state, board) {
    state.boards.push(board)
  },

  DELETE_COLUMN(state, idx) {
    state.board.columns.splice(idx, 1);
  },

  DELETE_COMMENT(state, idx) {
    state.comments.splice(idx, 1);
  },

  DELETE_TASK(state, data) {
    state.board.columns[data.columnIndex].tasks.splice(data.taskIndex, 1);
  },

  UPDATE_TASK(state, data) {
    state.board.columns[data.colIdx].tasks[data.taskIdx] = data.task
  },

  UPDATE_COLUMN(state, updateInfo){
    Vue.set(state.board.columns, updateInfo.idx, updateInfo.data)
  },

  SET_BOARD(state, board) {
    state.board = board
  },

  ADD_COLUMN(state, column) {
    state.board.columns.push(column);
  },

  ADD_COMMENT(state, comment) {
    state.comments.push(comment);
  },

  ADD_TASK(state, data) {
    for (let i = 0; i < state.board.columns.length; i++) {
      if(state.board.columns[i].id === data.column_id) {
        state.board.columns[i].tasks.push(data)
      }
    }
  },

  SET_COMMENTS(state, comments) {
    state.comments = comments
  }
}

export const actions = {
  async getBoardList() {
    const boards = await this.$axios.get(`/api/boards`)
    this.commit('SET_BOARDLIST', boards.data)
  },

  async getBoard(ctx, id) {
    const board = await this.$axios.get(`/api/boards/${id}`)
    ctx.commit('SET_BOARD', board.data)
  },

  async createBoard(ctx, boardName) {
    const newBoard = await this.$axios.post(`/api/boards`, {'name': boardName})
    ctx.commit('PUSH_BOARDLIST', newBoard.data)
  },

  async newBoardImage(ctx, { boardId, formData }) {
    const board = await this.$axios.post(`/api/boards/${boardId}/image`, formData)
    ctx.commit('SET_BOARD', board.data)
  },

  async updateBoard(ctx, { boardId, name }) {
    const board = await this.$axios.put(`/api/boards/${boardId}`, {'name': name})
    ctx.commit('SET_BOARD', board.data)
  },

  async addUserToBoard(ctx, data) {
    const users = await this.$axios.put(`/api/boards/${data.board_id}`, data)
    ctx.commit('SET_BOARD', users.data)
  },

  async removeUserFromBoard(ctx, data) {
    const board = await this.$axios.put(`/api/boards/${data.board_id}`, data)
    ctx.commit('SET_BOARD', board.data)
  },

  async removeBoard(ctx, boardId) {
    await this.$axios.delete(`/api/boards/${boardId}`)
  },

  async getComments(ctx, taskId) {
      const comments = await this.$axios.get(`/api/comments/${taskId}`)
      ctx.commit('SET_COMMENTS', comments.data)
  },

  async addColumn(ctx, newColumn) {
    const column = await this.$axios.post(`/api/columns`, newColumn)
    ctx.commit('ADD_COLUMN', column.data)
  },

  async addTask(ctx, data) {
    const task = await this.$axios.post(`/api/tasks`, data)
    ctx.commit('ADD_TASK', task.data)
  },

  async removeTask(ctx, data) {
    await this.$axios.delete(`/api/tasks/${data.taskId}`)
    ctx.commit('DELETE_TASK', data)
  },

  async removeColumn(ctx, data) {
    await this.$axios.delete(`/api/columns/${data.columnId}`)
    ctx.commit('DELETE_COLUMN', data.idx)
  },

  async updateTask(ctx, data) {
    const response = await this.$axios.put(`/api/tasks/${data.task.id}`, data.task)
    let info = {
      'colIdx': data.colIdx,
      'taskIdx': data.taskIdx,
      'task': response.data
    }
    ctx.commit('UPDATE_TASK', info)
  },

  async addComment(ctx, comment) {
    const comments = await this.$axios.post(`/api/comments`, comment)
    ctx.commit('ADD_COMMENT', comments.data)
  },

  async removeComment(ctx, data) {
    const comments = await this.$axios.delete(`/api/comments/${data.commentId}`)
    ctx.commit('DELETE_COMMENT', data.idx)
  },

  async updateColumn(ctx, {title, position, columnId, idx}) {
    let column = await this.$axios.put(`/api/columns/${columnId}`, {'title': title, 'position': position})

    let data = {
      'data': column.data,
      'idx': idx
    }
    ctx.commit('UPDATE_COLUMN', data)
  },

  async draggableColumn(ctx, {newPosition, columnId}) {
    const board = await this.$axios.put(`/api/columns/${columnId}`, {'position': newPosition})
    ctx.commit("SET_BOARD", board.data)
  },

  async draggableTask(ctx, {taskId, newPosition, idx}) {
    const column = await this.$axios.put(`/api/tasks/${taskId}`, {'position': newPosition})

    let data = {
      'data': column.data,
      'idx': idx
    }

    ctx.commit('UPDATE_COLUMN', data)
  }
}

export const getters = {
  getBoardList(state) {
    return state.boards
  },

  getBoard(state) {
    return state.board
  },

  getComments(state) {
    return state.comments
  },

  getColumnIndex: (state) => (columnId) => {
    return state.board.columns.findIndex(col => col.id === columnId);
  },

  getTaskIndex: (state) => (columnIndex, taskId) => {
    return state.board.columns[columnIndex].tasks.findIndex(task => task.id === taskId)
  }
}
